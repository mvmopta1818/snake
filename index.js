const snake = new Snake();
const scl = 25;
var ctx, width, height, food, key;
window.rate = 0;
window.speed = 0;
function setup() {
  createCanvas(400, 400);
  frameRate();
  pickLocation();
  keyPress();
  randomFood(window.speed);
}

function pickLocation() {
  const cols = Math.floor(width / scl);
  const rows = Math.floor(height / scl);
  food = createVector(Math.floor(random(cols)), Math.floor(random(rows)));
  food.mult(scl);
}
function randomFood(speed) {
  setTimeout(() => {
    if (window.speed == speed) {
      pickLocation();
      randomFood(speed);
    }
  }, 5000 - window.speed * 100);
}

function draw() {
  background(175, 228, 83);

  if (snake.Eat(food)) {
    pickLocation();
    window.speed = snake.total + 1;
    randomFood(window.speed);
  }
  snake.Death();
  snake.Update();
  snake.Show();
  snake.Move(key);

  drawImage("images/apple.png", food.x, food.y, scl, scl);
}

function background(r, g, b) {
  fill(r, g, b);
  rect(0, 0, width, height);
}

function fill(r, g, b) {
  if (g === undefined && b === undefined) {
    g = r;
    b = r;
  }

  ctx.fillStyle = `rgb(${r}, ${g}, ${b})`;
}

function drawImage(path, x, y, w, h) {
  const img = document.createElement("img");
  img.src = path;

  ctx.drawImage(img, x, y, w, h);
}

function rect(x, y, w, h) {
  ctx.fillRect(x, y, w, h);
}

function keyPress() {
  document.addEventListener("keydown", e => {
    key = e.keyCode;
  });
}

function createVector(x, y) {
  const obj = { x, y };
  obj.mult = function mult(m) {
    this.x *= m;
    this.y *= m;
  };
  return obj;
}

function constrain(n, low, high) {
  if (n > high) {
    return high;
  }

  if (n < low) {
    return low;
  }

  return n;
}

function dist(x1, y1, x2, y2) {
  return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
}

function random(min, max) {
  max = max || 1;
  return Math.random() * (max - min) + min;
}

function createCanvas(w, h) {
  if (w % 2 !== 0) w--;
  if (h % 2 !== 0) h--;

  const canvas = document.createElement("canvas");
  canvas.width = w;
  canvas.height = h;

  ctx = canvas.getContext("2d");
  width = canvas.width;
  height = canvas.height;
  document.body.appendChild(canvas);
}

function frameRate() {
  setTimeout(() => {
    draw();

    if (window.rate != 0) {
      frameRate(window.rate);
    }
  }, window.rate * 10);
}
setup();
