class Snake {
  constructor() {
    this.x = 0;
    this.y = 0;
    this.x_speed = 1;
    this.y_speed = 0;
    this.total = 0;
    this.tail = [];
    this.direction = "E";
  }

  Eat(pos) {
    const d = dist(this.x, this.y, pos.x, pos.y);
    if (d < scl) {
      this.total++;
      var puntaje = document.getElementById("puntaje");
      puntaje.innerText = this.total;
      return true;
    }

    return false;
  }

  Death() {
    for (var i = 0; i < this.tail.length; i++) {
      const pos = this.tail[i];
      const d = dist(this.x, this.y, pos.x, pos.y);
      if (d < 1) {
        var records = document.getElementById("record");
        var tr = document.createElement("tr");
        records.appendChild(tr);
        var td = document.createElement("td");
        td.innerText = prompt("nombre de usuario");
        var puntd = document.createElement("td");
        puntd.innerText = this.total;
        tr.appendChild(td);
        tr.appendChild(puntd);
        this.total = 0;
        this.tail = [];
        window.rate = 0;
        window.speed = 0;
      }
    }
  }

  Update() {
    for (var i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }
    if (this.total >= 1) {
      this.tail[this.total - 1] = createVector(this.x, this.y);
      this.tail[this.total - 1].direction = this.direction;
    }

    this.x = this.x + this.x_speed * scl;
    this.y = this.y + this.y_speed * scl;

    if (this.x >= width) {
      this.x = 0;
    } else if (this.x < 0) {
      this.x = width;
    }

    if (this.y >= height) {
      this.y = 0;
    } else if (this.y < 0) {
      this.y = height;
    }
  }

  Show() {
    fill(0);
    if (this.tail.length > 0) {
      drawImage(
        `images/tails/${this.tail[0].direction}.png`,
        this.tail[0].x,
        this.tail[0].y,
        scl,
        scl
      );
    }
    for (var i = 1; i < this.tail.length; i++) {
      drawImage(
        `images/bodys/${this.tail[i].direction}.png`,
        this.tail[i].x,
        this.tail[i].y,
        scl,
        scl
      );
    }

    drawImage(`images/heads/${this.direction}.png`, this.x, this.y, scl, scl);
  }

  Move(keyCode) {
    switch (keyCode) {
      case 37:
        if (this.y_speed !== 0) {
          this.direction = "O";
          this.x_speed = -1;
          this.y_speed = 0;
        }
        break;
      case 39:
        if (this.y_speed !== 0) {
          this.direction = "E";
          this.x_speed = 1;
          this.y_speed = 0;
        }
        break;
      case 38:
        if (this.x_speed !== 0) {
          this.direction = "N";
          this.x_speed = 0;
          this.y_speed = -1;
        }
        break;
      case 40:
        if (this.x_speed !== 0) {
          this.direction = "S";
          this.x_speed = 0;
          this.y_speed = 1;
        }
        break;
      default:
    }
  }
}
